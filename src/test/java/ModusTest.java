import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ModusTest {

    private final BacadanSimpan baca = new BacadanSimpan();

    @Test
    @DisplayName("Test cari modus sesuai")
    void testModus () throws IOException {

        List<Integer> listNilai = new ArrayList<>();
        baca.BacaFile(listNilai);

        int counter = 0;
        int countertmp = 0;
        int hasil = 0;
        for (int i=0; i<=10; i++) {
            for (Integer s : listNilai) {
                if (i == s) {
                    counter++;
                }
                if (counter > countertmp) {
                    hasil = i;
                    countertmp = counter;
                }
            }
            counter = 0;
        }
        assertEquals("7", String.valueOf(hasil));
    }
}
