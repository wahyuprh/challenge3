import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MenuTest {
    Menu menu = new Menu();

    @Test
    @DisplayName("Tes input menu valid")
    void testmenuAwal() {
        WriteFreq writeFreq = new WriteFreq();
        WriteMean writeMean = new WriteMean();
        menu.menuTop();
        System.out.println("\nLetakkan file csv dengan nama file data_sekolah.csv di direktori\nberikut: C:/temp/direktori\n");
        System.out.println("pilih menu:");
        System.out.println("1. Buat file txt untuk menampilkan frekuensi nilai");
        System.out.println("2. Buat file txt untuk menampilkan nilai rata-rata, median dan modus");
        System.out.println("3. Buat file kedua file");
        System.out.println("0. Keluar");
        System.out.print("\npilihan anda : ");
        String pilihMenu = "2";
        System.out.println();
        BlurTulis.pilihan = pilihMenu;
        switch (pilihMenu) {
            case "0":
            case "1":
                break;
            case "2":
            case "3":
                break;
            default:
        }
    }

    @Test
    @DisplayName("Tes input menu tidak valid")
    void testmenuAwal2() {
        WriteFreq writeFreq = new WriteFreq();
        WriteMean writeMean = new WriteMean();
        menu.menuTop();
        System.out.println("\nLetakkan file csv dengan nama file data_sekolah.csv di direktori\nberikut: C:/temp/direktori\n");
        System.out.println("pilih menu:");
        System.out.println("1. Buat file txt untuk menampilkan frekuensi nilai");
        System.out.println("2. Buat file txt untuk menampilkan nilai rata-rata, median dan modus");
        System.out.println("3. Buat file kedua file");
        System.out.println("0. Keluar");
        System.out.print("\npilihan anda : ");
        String pilihMenu = "Y";
        System.out.println();
        BlurTulis.pilihan = pilihMenu;
        switch (pilihMenu) {
            case "0":
            case "1":
                break;
            case "2":
            case "3":
                break;
            default:
        }
    }
}
