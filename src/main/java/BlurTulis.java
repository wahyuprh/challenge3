public abstract class BlurTulis {

    protected static String pilihan;
    protected Menu menu = new Menu();
    protected String output1 = "data_sekolah_modus.txt";
    protected String output2 = "data_sekolah_modus_median.txt";

    abstract void tulis ();

    void generate(String file){
        menu.menuTop();
        System.out.println("\nFile \"" +file+"\" telah digenerate\ndi difolder gaess\nsilahkan cek.\n");
    }

    void generate(String file1, String file2){
        menu.menuTop();
        System.out.println("\nFile \""+file1+"\" dan \""+file2+"\"\ntelah digenerate\ndi folder gaess\nsilahkan cek.\n");
    }
}
