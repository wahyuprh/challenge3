import wahyuprihantono.intrface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Modus implements intrface {

    private final List<Integer> listNilai = new ArrayList<>();
    private final BacadanSimpan baca = new BacadanSimpan();

    public String hitung() throws IOException {

        baca.BacaFile(listNilai);

        int counter = 0;
        int countertmp = 0;
        int hasil = 0;
        for (int i=0; i<=10; i++){
            for (Integer s : listNilai) {
                if (i == s) {
                    counter++;
                }
                if (counter > countertmp) {
                    hasil = i;
                    countertmp = counter;
                }
            }
            counter = 0;
        }
        return String.valueOf(hasil);
    }
}
