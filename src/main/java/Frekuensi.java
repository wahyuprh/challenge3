import wahyuprihantono.intrface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Frekuensi implements intrface{
    private final List<Integer> listNilai = new ArrayList<>();
    private final BacadanSimpan baca = new BacadanSimpan();

    public String hitung() throws IOException {

        baca.BacaFile(listNilai);

        StringBuilder sb = new StringBuilder();
        sb.append("Nilai\t\t| Frekuensi\n").append("----------------|----------\n");
        int counter = 0;
        //Untuk kurang dari 6
        for (Integer s : listNilai) {
            if (s < 6) {
                counter++;
            }
        }
        sb.append("Kurang dari 6\t| ").append(counter).append("\n");
        //Untuk nilai 6 keatas
        for (int i = 6; i<=10; i++){
            counter =0;
            for (Integer s : listNilai) {
                if (i == s) {
                    counter++;
                }
            }
            sb.append(i).append("\t\t| ").append(counter).append("\n");
        }
        return sb.toString();
    }
}
