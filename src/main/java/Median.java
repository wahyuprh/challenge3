import wahyuprihantono.intrface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Median implements intrface {

    private final List<Integer> listNilai = new ArrayList<>();
    private final BacadanSimpan baca = new BacadanSimpan();

    public String hitung() throws IOException {

        baca.BacaFile(listNilai);

        List <Integer> listSorted = listNilai.stream().sorted().collect(Collectors.toList());

        float hasil;
        int indexMedian = listSorted.size()/2;
        if (listSorted.size()%2==0){
            hasil = listSorted.get(((indexMedian + (indexMedian + 1)) / 2));
        } else {
            hasil = listSorted.get(indexMedian+1);
        }
        return String.valueOf(hasil);
    }
}
