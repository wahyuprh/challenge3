import wahyuprihantono.intrface;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Mean implements intrface {

    private final List<Integer> listNilai = new ArrayList<>();
    private final BacadanSimpan baca = new BacadanSimpan();

    public String hitung() throws IOException {

        baca.BacaFile(listNilai);

        float total = 0;
        for (int t : listNilai){
            total = total + t;
        }
        float hasil = total/ listNilai.size();
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(hasil);
    }
}