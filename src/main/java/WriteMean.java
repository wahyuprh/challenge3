import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteMean extends BlurTulis{
    private final Mean mean = new Mean();
    private final Median median = new Median();
    private final Modus modus = new Modus();

    @Override
    void tulis() {
        try {
            File file = new File("D:\\Challeng-3\\File\\data_sekolah_modus_median.txt");
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write ("Berikut Hasil Pengolahan Nilai:\n\n");
            bwr.write("Berikut hasil data nilai:\n");
            bwr.write("Mean" + "\t\t" + ":" + "\t" + mean.hitung()+"\n");
            bwr.write("Median" + "\t\t" + ":" + "\t" + median.hitung()+"\n");
            bwr.write("Modus" + "\t\t" + ":" + "\t" + modus.hitung());
            bwr.newLine();
            bwr.flush();
            bwr.close();
            if (pilihan.equals("3")){
                generate(output1,output2);
            } else {
                generate(output2);
            }
        }
        catch (IOException ioe){
            menu.menuTop();
            System.out.print(ioe);
            menu.failedGenerate();
        }
        finally {
            menu.back();
        }
    }
}